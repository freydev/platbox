var express = require('express');
var bodyParser = require('body-parser');
var crypto = require('crypto');
var app = express()

var port = 9001;
var salt = 'platbox';
var password = '1235';

app.use(bodyParser.urlencoded())

function setLength(val, len) {
  result = val.split('');
  var index = 0;

  while (result.length < len) {
    if (index > val.length - 1) index = 0;
    result.push(val[index++])
  }

  return result.join('');
}

function encode(data, salt) {
  var tmpSolt = setLength(salt, data.length)
  var tmpData = data.split('');

  tmpData = tmpData.map(function (chr, i) {
    return String.fromCharCode(chr.charCodeAt() ^ tmpSolt[i].charCodeAt())
  })

  return tmpData.join('')
}

app.post('/check', function (req, res) {
  var pass = crypto.createHash('md5').update(req.body.pass).digest("hex");
  var truePass = crypto.createHash('md5').update(password).digest("hex");
  if (pass == truePass)
    res.status(200).send(pass)
  else res.status(202).send()
})

app.post('/form', function (req, res) {
  var params = encode(new Buffer(req.body.params, 'base64').toString('ascii'), salt)
  if (!/__platbox__/.test(params)) {
    return res.status(400).send()
  }

  params = JSON.parse(params.replace('__platbox__', ''));

  function objToStyle(obj) {
    var str = JSON.stringify(obj);
    return str.replace(/[{}]/g, '')
      .replace(/,/g, ';')
      .replace(/"/g, '')
  }

  res.send(  
    '<style>html, body {display: inline; margin: 0; padding: 0;</style>' +
    '<input id="pass" style="' + objToStyle(params.style) + '" type="password" name="password">' +
    '<script>' +
      'var timer;' +
      'document.getElementById("pass").onkeydown = function(ev){' +
        'if (ev.keyCode == 13) {' +
          'parent.postMessage("submit", "*");' +
          'return false' +
        '}' + 
        'clearTimeout(timer);' +
        'timer = setTimeout(function(){' +
          'var req = new XMLHttpRequest();' +
          'req.open("POST", "/check", true);' +
          'req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");' +
          'req.send("pass="+this.value);' +
          'req.onload=function(){' +
          'if (req.status === 200) parent.postMessage(req.responseText, "*");' +
          'else parent.postMessage("", "*")' +
          '};' +
        '}.bind(this), 800)' +
      '}' +
    '</script>'
  )
})

app.listen(port, function () {
  // console.log(
  //   new Buffer(encode(JSON.stringify({style: {width: '100%',
  //   height: '100%',
  //   border: 0,
  //   outline: 0,
  //   'font-size': '18px',
  //   padding: '0px 10px',
  //   'box-sizing': 'border-box'}}) + '__platbox__', salt)).toString('base64')
  // )
  console.log('Test server on port ' + port)
})
