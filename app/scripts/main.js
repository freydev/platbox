var form = document.querySelector('#testForm');

setTimeout(function () {
  const iframe = document.querySelector('iframe'),
    requestForm = document.createElement('form');

  document.body.appendChild(requestForm)    
  requestForm.action = 'http://localhost:9001/form';
  requestForm.method = 'post';
  requestForm.target = 'password';

  var input = document.createElement('input')
  input.type = 'hidden';
  input.name = 'params';
  input.value = iframe.getAttribute('params');  
  requestForm.appendChild(input)
  
  requestForm.submit();
  iframe.onload = function () {
    form.classList.add('show');
  }
}, 10);


window.addEventListener('message', ev => {
  if (ev.data == 'submit' && form.passwordHash.value && form.login.value) {
    //form.submit()
    alert('success')
  } else {
    form.passwordHash.value = ev.data;
    if (form.passwordHash.value && form.login.value)
      form.querySelector('input[type="submit"]').classList.remove('disabled');
    else
      form.querySelector('input[type="submit"]').classList.add('disabled');
  }
})

form.onkeyup = function (ev) {
  if (form.passwordHash.value && form.login.value)
    form.querySelector('input[type="submit"]').classList.remove('disabled');
  else
    form.querySelector('input[type="submit"]').classList.add('disabled');
}

form.onsubmit = function (ev) {
  return (function () {
    ev.preventDefault();

    if (!this.passwordHash.value || !this.login.value) {
      return false
    };

    alert('success')
    return false;
  }.bind(this))();
}
